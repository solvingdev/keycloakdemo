var express = require('express');
var router = express.Router();
var Keycloak = require('keycloak-connect');
var keycloak = new Keycloak({}); // search for keycloak.json

/* GET home. */
router.get('/', function(req, res, next) {
    res.json({ message: 'hooray! welcome to our api!' });
});

/* GET secured. */
router.get('/secure', keycloak.protect(), function(req, res, next) {
    res.json({ message: 'VIP only!' });
});

/* GET secured. */
router.get('/admin', keycloak.enforcer('admin_policy'), function(req, res, next) {
    res.json({ message: 'admin only!' });
});

module.exports = router;
