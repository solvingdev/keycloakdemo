package it.solvingteam.keycloakexample;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller
public class InsecureAccessController {
    @GetMapping("/insecure")
    public String insecure(Principal principal, Model model) {
        if (principal != null) {
            model.addAttribute("username", principal.getName());
        }
        return "insecure";
    }
}
