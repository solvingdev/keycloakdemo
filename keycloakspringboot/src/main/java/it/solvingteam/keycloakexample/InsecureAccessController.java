package it.solvingteam.keycloakexample;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class InsecureAccessController {
    @GetMapping("/insecure")
    public String insecure() {
        return "insecure";
    }
}
