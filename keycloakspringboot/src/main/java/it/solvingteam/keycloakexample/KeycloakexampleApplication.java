package it.solvingteam.keycloakexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KeycloakexampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(KeycloakexampleApplication.class, args);
	}

}
